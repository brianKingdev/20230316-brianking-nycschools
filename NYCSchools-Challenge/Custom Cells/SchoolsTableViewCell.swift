//
//  SchoolsTableViewCell.swift
//  NYCSchools-Challenge
//
//  Created by brian.b.king on 3/15/23.
//

import UIKit

/// Custom TableView Cell class to use for Schools List View
class SchoolsTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var studentCountLabel: UILabel!
    
    // MARK: - Variables
    var school: School? {
        // update UI once school variable is updated
        didSet {
            if let school = self.school {
                self.updateUI(school: school)
            }
        }
    }

    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    // MARK: - Private Functions
    private func updateUI(school: School) {
        self.nameLabel.text = school.school_name
        self.locationLabel.text = school.cityAndStateAndBorough
        
        guard let studentCount = school.total_students else { return }
        self.studentCountLabel.text = "\(studentCount) Students"
    }
}
