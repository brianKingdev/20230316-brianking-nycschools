//
//  Constants.swift
//  NYCSchools-Challenge
//
//  Created by brian.b.king on 3/15/23.
//

import Foundation

// Class to store all constants to use project wide
struct Constants {
    static let schoolDataURL = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    static let satsDataURL = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
}
