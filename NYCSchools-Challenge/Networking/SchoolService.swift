//
//  SchoolService.swift
//  NYCSchools-Challenge
//
//  Created by brian.b.king on 3/15/23.
//

import Foundation

/// Class for all school data networking
final class SchoolService {
    func fetchSchoolData(from url: String, completion: @escaping(Result<[School], Error>) -> Void) {
        
        // Create task using URLSession to fetch json data for schools
        let task = URLSession.shared.dataTask(with: URL(string: url)!, completionHandler: { data, response, error in
            
            // check to ensure data came back successfully without an error, otherwise handle error
            guard let jsonData = data, error == nil else {
                print("Something went wrong trying to fetch school data. Error: \(error!.localizedDescription)")
                completion(.failure(error!))
                return
            }
            
            // handle the resulting json data and attempt to decode it into our school model
            let result = Result(catching: { try JSONDecoder().decode([School].self, from: jsonData)})

            // pass our new result into the completion handler
            completion(result)
        })
        
        task.resume()
    }
    
    func fetchSATData(from url: String, completion: @escaping(Result<[SATData], Error>) -> Void) {
        
        // Create task using URLSession to fetch json data for SAT data
        let task = URLSession.shared.dataTask(with: URL(string: url)!, completionHandler: { data, response, error in
            
            // check to ensure data came back successfully without an error, otherwise handle error
            guard let jsonData = data, error == nil else {
                print("Something went wrong trying to fetch SAT data. Error: \(error!.localizedDescription)")
                completion(.failure(error!))
                return
            }
            
            // handle the resulting json data and attempt to decode it into the SAT data model
            let result = Result(catching: { try JSONDecoder().decode([SATData].self, from: jsonData)})
            
            // pass the new result into the completion handler
            completion(result)
        })
        
        task.resume()
    }
}

