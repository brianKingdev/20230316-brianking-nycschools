//
//  School.swift
//  NYCSchools-Challenge
//
//  Created by brian.b.king on 3/15/23.
//

import Foundation

/// Model for school that represents the *needed* data coming from the JSON payload
struct School: Codable {
    var dbn: String
    var school_name: String
    var city: String?
    var zip: String?
    var state_code: String?
    var primary_address_line_1: String?
    var overview_paragraph: String?
    var academicOpportuniities1: String?
    var academicOpportuniities2: String?
    var phone_number: String?
    var school_email: String?
    var website: String?
    var total_students: String?
    var school_sports: String?
    var borough: String?
    
    var cityAndStateAndBorough: String {
        return "\(city ?? "N/A"), \(state_code ?? "N/A") • \(borough ?? "N/A")"
    }
    
    var fullAddress: String {
        return "\(primary_address_line_1 ?? "N/A") \(city ?? "N/A"), \(state_code ?? "N/A") • \(borough ?? "N/A")"
    }
}

