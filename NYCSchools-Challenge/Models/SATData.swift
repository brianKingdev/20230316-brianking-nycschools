//
//  SATData.swift
//  NYCSchools-Challenge
//
//  Created by brian.b.king on 3/16/23.
//

import Foundation

// Model for SAT Data from JSON Payload via API
struct SATData: Codable {
    var dbn: String
    var school_name: String
    var num_of_sat_test_takers: String?
    var sat_critical_reading_avg_score: String?
    var sat_math_avg_score: String?
    var sat_writing_avg_score: String?
}
