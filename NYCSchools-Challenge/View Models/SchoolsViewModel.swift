//
//  SchoolsViewModel.swift
//  NYCSchools-Challenge
//
//  Created by brian.b.king on 3/15/23.
//

import Foundation

/// This class serves as the business logic layer for the Schools ViewController
class SchoolsViewModel {
    
    // MARK: - Variables
    let schoolService = SchoolService()
    
    var isFetchedSchoolsSuccessful = false
    var isFetchedSATDataSuccessful = false
    
    /// If I had more time I would keep the school/SAT data in the view model and retrieve it using the ViewController and test if the data exists or not
    // var schools = [School]()
    // var satData = [SATData]()
    
    // MARK: - Functions
    func fetchSchools(completion: @escaping (Result<[School], Error>) -> Void) {
        
        schoolService.fetchSchoolData(from: Constants.schoolDataURL, completion: { [weak self] results in
            
            // handle the success and failure cases for the results of the data call
            switch results {
            case .success:
                // included for unit testing
                self?.isFetchedSchoolsSuccessful = true
            case .failure:
                // included for unit testing
                self?.isFetchedSchoolsSuccessful = false
            }
            
            completion(results)
        })
    }
    
    func fetchSATData(completion: @escaping (Result<[SATData], Error>) -> Void) {
        
        schoolService.fetchSATData(from: Constants.satsDataURL, completion: { [weak self] results in
            
            // handle the success and failure cases for the results of the data call
            switch results {
            case .success:
                // included for unit testing
                self?.isFetchedSATDataSuccessful = true
            case .failure:
                // included for unit testing
                self?.isFetchedSATDataSuccessful = false
            }
            
            completion(results)
        })
    }
}
