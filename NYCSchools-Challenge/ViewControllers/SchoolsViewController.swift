//
//  SchoolsViewController.swift
//  NYCSchools-Challenge
//
//  Created by brian.b.king on 3/15/23.
//

import UIKit

/// Main class that shows list of NYC schools using data from API
class SchoolsViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var schoolsTableView: UITableView!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    // MARK: - Private Variables
    private var viewModel = SchoolsViewModel()
    private var schools = [School]()
    private var selectedSchool: School?
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchSchoolData()
        setTableViewProperties()
        setDelegatesAndDatasources()
    }
    
    // MARK: - Private Functions
    private func setDelegatesAndDatasources() {
        self.schoolsTableView.delegate = self
        self.schoolsTableView.dataSource = self
    }
    
    private func setTableViewProperties() {
        schoolsTableView.rowHeight = UITableView.automaticDimension
        schoolsTableView.estimatedRowHeight = 58
    }
    
    internal override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        /// function to pass data to detail view
        if segue.identifier == "schoolDetailViewSegue" {
            
            // set detail view as the segue's desitation
            let schoolDetailController = segue.destination as! SchoolDetailViewController
            
            // unwrap the selected school to pass to the detail view
            guard let school = self.selectedSchool else { return }
            
            // pass the unwrapped selected school to the detail view
            schoolDetailController.school = school
            schoolDetailController.viewModel = self.viewModel
        }
    }
    
    private func fetchSchoolData() {
        activityIndicatorView.startAnimating()
        
        // fetch school list from viewmodel
        viewModel.fetchSchools(completion: { [weak self] results in
            
            // handle result cases
            switch results {
            case .success(let schools):
                
                // Alphabetically sort the school list by school name
                let sortedSchools = schools.sorted {$0.school_name < $1.school_name}
                self?.schools = sortedSchools
                
                // update UI components on main thread
                DispatchQueue.main.async {
                    self?.activityIndicatorView.stopAnimating()
                    self?.schoolsTableView.reloadData()
                }
                
            case .failure(let error):
                // print error for cognizance
                print("Failed to retrieve School Data. Error: \(error.localizedDescription)")
                
                // Display an alert to user explaining error
                DispatchQueue.main.async {
                    self?.activityIndicatorView.stopAnimating()
                    self?.presentErrorAlert()
                }
            }
        })
    }
    
    private func presentErrorAlert() {
        let alert = UIAlertController.init(title: "Oops!", message: "Sorry, there was an issue loading the school data. Please try again later.", preferredStyle: .alert)
        self.present(alert, animated: true, completion: nil)
    }
}

/// Extend tableview for placeholder capabilities when no data is available
extension UITableView {
    func setEmptyView(title: String, message: String) {
        let emptyView = UIView(frame: CGRect(x: self.center.x, y: self.center.y, width: self.bounds.size.width, height: self.bounds.size.height))
        let titleLabel = UILabel()
        let messageLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.textColor = UIColor.lightGray
        titleLabel.font = UIFont(name: "HelveticaNeue-Medium", size: 18)
        messageLabel.textColor = UIColor.lightGray
        messageLabel.font = UIFont(name: "HelveticaNeue-Light", size: 12)
        emptyView.addSubview(titleLabel)
        emptyView.addSubview(messageLabel)
        titleLabel.centerYAnchor.constraint(equalTo: emptyView.centerYAnchor).isActive = true
        titleLabel.centerXAnchor.constraint(equalTo: emptyView.centerXAnchor).isActive = true
        messageLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20).isActive = true
        messageLabel.leftAnchor.constraint(equalTo: emptyView.leftAnchor, constant: 20).isActive = true
        messageLabel.rightAnchor.constraint(equalTo: emptyView.rightAnchor, constant: -20).isActive = true
        titleLabel.text = title
        messageLabel.text = message
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        
        self.backgroundView = emptyView
        self.separatorStyle = .none
    }
    
    func restore() {
        self.backgroundView = nil
        self.separatorStyle = .singleLine
    }
}

/// Extend the class for TableView Delegate Functions
extension SchoolsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // store the selected school
        self.selectedSchool = schools[indexPath.row]
        
        // check to ensure selected school was stored successfully
        guard let _ = self.selectedSchool else { return }
        
        // navigate to detail view
        self.performSegue(withIdentifier: "schoolDetailViewSegue", sender: self)
    }
}

/// Extend the class for TableView Data Functions
extension SchoolsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        // Check for schools count then return the number of schools in the array or show placeholder
        if self.schools.count == 0 {
            tableView.setEmptyView(title: "No Data Available", message: "")
        } else {
            tableView.restore()
        }
        
        return self.schools.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create our custom cell for schools list
        let cell: SchoolsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "schoolsCell", for: indexPath) as! SchoolsTableViewCell
        
        // give the cell a school object
        cell.school = schools[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        // allow for self-sizing rows based on content
        return UITableView.automaticDimension
    }
}
