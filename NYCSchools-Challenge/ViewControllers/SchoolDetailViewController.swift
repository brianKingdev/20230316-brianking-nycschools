//
//  SchoolDetailViewController.swift
//  NYCSchools-Challenge
//
//  Created by brian.b.king on 3/16/23.
//

import UIKit

/// Class to show School Details once a school has been selected from the Main View
class SchoolDetailViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mathScoreLabel: UILabel!
    @IBOutlet weak var readingScoreLabel: UILabel!
    @IBOutlet weak var writingScoreLabel: UILabel!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var phoneNumberTextView: UITextView!
    @IBOutlet weak var emailTextView: UITextView!
    @IBOutlet weak var addressTextView: UITextView!
    @IBOutlet weak var websiteTextView: UITextView!
    
    // MARK: - Variables
    var school: School!
    var viewModel: SchoolsViewModel!
    var satData: SATData? {
        didSet {
            updateUIComponents()
        }
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchSATData()
        updateUIComponents()
    }
    
    // MARK: - Private Functions
    private func updateUIComponents() {
        nameLabel.text = self.school.school_name
        descriptionLabel.text = self.school.overview_paragraph ?? "N/A"
        phoneNumberTextView.text = "P: \(self.school.phone_number ?? "N/A")"
        emailTextView.text = "E: \(self.school.school_email ?? "N/A")"
        websiteTextView.text = "\(self.school.website ?? "")"
        addressTextView.text = self.school.fullAddress
        
        guard let satData = self.satData else { return }
        self.mathScoreLabel.text = satData.sat_math_avg_score
        self.readingScoreLabel.text = satData.sat_critical_reading_avg_score
        self.writingScoreLabel.text = satData.sat_writing_avg_score
    }
    
    private func fetchSATData() {
        // show activity indicator
        activityIndicatorView.startAnimating()
        
        // fetch satData from viewmodel
        viewModel.fetchSATData(completion: { [weak self] results in
            
            // handle result cases
            switch results {
            case .success(let satData):
                
                // update UI components asynchronously on the main thread
                DispatchQueue.main.async {
                    self?.filterSATDataForSchool(satData: satData)
                    self?.activityIndicatorView.stopAnimating()
                }
                
            case .failure(let error):                
                // print error for cognizance
                print("Failed to retrieve SATData for school: \(self?.school.school_name ?? "N/A"). Error: \(error.localizedDescription)")
                
                // Display an alert to user explaining error
                DispatchQueue.main.async {
                    self?.activityIndicatorView.stopAnimating()
                    self?.presentErrorAlert()
                }
            }
        })
    }
    
    private func presentErrorAlert() {
        let alert = UIAlertController.init(title: "Oops!", message: "There was an issue loading the SAT data for this particular school.", preferredStyle: .alert)
        self.present(alert, animated: true, completion: nil)
    }
    
    private func filterSATDataForSchool(satData: [SATData]) {
        let filteredData = satData.filter { $0.dbn == self.school.dbn}
        guard filteredData.count < 2 else { return }
        guard let matchingData = filteredData.first else { return }
        self.satData = matchingData
    }
}
